FROM alpine/git:2.45.2

ENTRYPOINT ["git", "diff", "--exit-code"]
