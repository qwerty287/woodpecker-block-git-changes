---
name: Block Git changes
author: qwerty287
description: Plugin to block uncommited changes in the Git repository
tags: [git, changes]
containerImage: codeberg.org/qwerty287/woodpecker-block-git-changes
containerImageUrl: https://codeberg.org/qwerty287/-/packages/container/woodpecker-block-git-changes/latest
url: https://codeberg.org/qwerty287/woodpecker-block-git-changes
icon: https://raw.githubusercontent.com/appleboy/drone-git-push/master/images/logo.svg
---

Plugin that fails if the Git repository contains any uncommited changes.

## Usage

```yaml
pipeline:
	block-changes:
		image: codeberg.org/qwerty287/woodpecker-block-git-changes
```

This will never fail because there isn't any change done before.

```yaml
pipeline:
	echo:
		image: alpine
		commands:
			- echo "hello world" > README.md

    block-changes:
      image: codeberg.org/qwerty287/woodpecker-block-git-changes
```

This will fail if your README.md doesn't already contain `hello world`.
